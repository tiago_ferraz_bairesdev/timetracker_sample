from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.db.models import Q

class Project(models.Model):
    name = models.CharField(max_length=20)
    code = models.CharField(max_length=2)

    def __str__(self):
       return self.name

class Manager(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, limit_choices_to=Q(groups__name__in=['Manager']))
    assigned_projects = models.ManyToManyField(Project)

    def __str__(self):
       return f'{self.user.first_name} {self.user.last_name}' 

class Developer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, limit_choices_to=Q(groups__name__in=['Developer']))
    assigned_projects = models.ManyToManyField(Project)

    def __str__(self):
       return f'{self.user.first_name} {self.user.last_name}'

class TaskCategory(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class TaskDescription(models.Model):
    category = models.ForeignKey(TaskCategory, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.name} ({self.category})'

class Appointment(models.Model):
    developer = models.ForeignKey(Developer, on_delete=models.CASCADE, related_name='developer')
    date = models.DateField('Date')
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    hours = models.IntegerField()
    category = models.ForeignKey(TaskCategory, on_delete=models.CASCADE)
    description = models.ForeignKey(TaskDescription, on_delete=models.CASCADE)
    comments = models.TextField(max_length=500)
    manager = models.ForeignKey(Manager, on_delete=models.CASCADE, related_name='manager')
    
    
    def __str__(self):
       return f'{self.developer} worked {self.hours} hours in {self.date} for {self.project}'