from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Project, Appointment, Developer, Manager, TaskCategory, TaskDescription

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')


admin.site.register(Appointment)
admin.site.register(Developer)
admin.site.register(Manager)
admin.site.register(TaskCategory)
admin.site.register(TaskDescription)
