from django.urls import path

from . import views

urlpatterns = [
    path('', views.AppointmentListView.as_view(), name='index'),
    path('appointment/new', views.AppointmentCreateView.as_view(), name='new_appointment'),
    path('appointment/<int:appointment_pk>/edit', views.AppointmentUpdateView.as_view(), name='edit_appointment'),
    path('appointment/<int:appointment_pk>/delete', views.AppointmentDeleteView.as_view(), name='delete_appointment'),
]